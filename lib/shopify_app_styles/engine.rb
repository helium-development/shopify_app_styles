module ShopifyAppStyles
  class Engine < ::Rails::Engine
    initializer :assets, group: :all do
      ::Rails.application.config.assets.precompile += ['app_styles/*', 'app_styles/*']
    end
  end
end
