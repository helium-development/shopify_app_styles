window.App = window.App || {};

App.layout = {
  condense: function() {
    $('body').addClass('condensed');
  },
  expand: function() {
    $('body').removeClass('condensed');
  },
  toggle: function() {
    $('body').toggleClass('condensed');
  }
}
